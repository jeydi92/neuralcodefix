import random
import sys
from os import makedirs
from os.path import join, exists

import numpy as np
import torch
from torch import optim, nn
from torch.utils.tensorboard import SummaryWriter
from torchtext.data import Field, TabularDataset, BucketIterator
from torchtext.data.metrics import bleu_score

from inference import translate_sentence, create_attention_figure, compute_metrics
from models.attention import Attention
from models.decoder import Decoder
from models.encoder import Encoder
from models.seq2seq import Seq2Seq
from training import train, evaluate
from utils.metrics import corrects

SEED = 1337

project = sys.argv[1]
size = sys.argv[2]
exp = sys.argv[3]
experiment_name = f"{project}-{size}-{exp}"
lr = 0.0002
N_EPOCHS = 200
BATCH_SIZE = 32

dataset_path = f"datasets/tufano/{project}/"

if not exists('best_models'):
    makedirs('best_models')

writer = SummaryWriter(f"runs/{experiment_name}")

random.seed(SEED)
np.random.seed(SEED)
torch.manual_seed(SEED)
torch.cuda.manual_seed(SEED)
torch.backends.cudnn.deterministic = True

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

CODE = Field(init_token='<sos>',
             eos_token='<eos>',
             lower=True,
             include_lengths=True)

train_data = TabularDataset(
    path=join(dataset_path, f'train_{size}.json'), format='json',
    fields={'abstract_before': ('src', CODE),
            'abstract_after': ('trg', CODE)  # ,
            # 'operations': ('operations', OP)
            })

CODE.build_vocab(train_data, min_freq=1)

valid_data = TabularDataset(
    path=join(dataset_path, f'eval_{size}.json'), format='json',
    fields={'abstract_before': ('src', CODE),
            'abstract_after': ('trg', CODE)  # ,
            # 'operations': ('operations', OP)
            })

test_data = TabularDataset(
    path=join(dataset_path, f'test_{size}.json'), format='json',
    fields={'abstract_before': ('src', CODE),
            'abstract_after': ('trg', CODE)  # ,
            # 'operations': ('operations', OP)
            })

train_iterator, valid_iterator, test_iterator = BucketIterator.splits(
    (train_data, valid_data, test_data),
    batch_size=BATCH_SIZE,
    sort_within_batch=True,
    sort_key=lambda x: len(x.src),
    device=device, shuffle=True)

INPUT_DIM = len(CODE.vocab)
OUTPUT_DIM = len(CODE.vocab)
ENC_EMB_DIM = 512
DEC_EMB_DIM = 512
ENC_HID_DIM = 256
DEC_HID_DIM = 256
ENC_DROPOUT = 0.2
DEC_DROPOUT = 0.2
SRC_PAD_IDX = CODE.vocab.stoi[CODE.pad_token]

attn = Attention(ENC_HID_DIM, DEC_HID_DIM)
enc = Encoder(INPUT_DIM, ENC_EMB_DIM, ENC_HID_DIM, DEC_HID_DIM, ENC_DROPOUT)
dec = Decoder(OUTPUT_DIM, DEC_EMB_DIM, ENC_HID_DIM, DEC_HID_DIM, DEC_DROPOUT, attn)

model = Seq2Seq(enc, dec, SRC_PAD_IDX, device).to(device)


def init_weights(m):
    for name, param in m.named_parameters():
        if 'weight' in name:
            nn.init.normal_(param.data, mean=0, std=0.01)
        else:
            nn.init.constant_(param.data, 0)


model.apply(init_weights)


def count_parameters(model):
    return sum(p.numel() for p in model.parameters() if p.requires_grad)


print(f'The model has {count_parameters(model):,} trainable parameters')

optimizer = optim.Adam(model.parameters(), lr=lr, eps=0.0000008)
TRG_PAD_IDX = CODE.vocab.stoi[CODE.pad_token]

criterion = nn.CrossEntropyLoss(ignore_index=TRG_PAD_IDX)

CLIP = 1

best_valid_loss = float('inf')
max_valid_corr = 0

example_idx = random.randint(1, len(train_data))

src = train_data.examples[example_idx].src
trg = train_data.examples[example_idx].trg
writer.add_text("Source", " ".join(src), 0)
writer.add_text("Target", " ".join(trg), 0)

max_lens = {"medium": 100, "small": 50}

train_metrics = {"BLUE": bleu_score, "Corrects": corrects}
for epoch in range(N_EPOCHS):

    train_loss = train(model, train_iterator, optimizer, criterion, CLIP)
    valid_loss = evaluate(model, valid_iterator, criterion)
    test_loss = evaluate(model, test_iterator, criterion)

    writer.add_scalar("Loss/Train", train_loss, epoch)
    writer.add_scalar("Loss/Valid", valid_loss, epoch)
    writer.add_scalar("Loss/Test", test_loss, epoch)

    if valid_loss < best_valid_loss:
        best_valid_loss = valid_loss
        torch.save(model.state_dict(), f'best_models/{experiment_name}.pt')

        # if epoch == 0 or epoch % 5 == 0:
    translation, attention = translate_sentence(src, CODE, CODE, model, device)
    f = create_attention_figure(src, translation, attention)

    writer.add_figure("Attention", f, global_step=epoch, close=True)
    writer.add_text("Predicted", " ".join(translation), epoch)

    scores_valid = compute_metrics(valid_data, CODE, CODE, model, device, train_metrics, max_len=max_lens[size])
    scores_test = compute_metrics(test_data, CODE, CODE, model, device, train_metrics, max_len=max_lens[size])

    valid_bleu_score = scores_valid['BLUE'] * 100
    test_bleu_score = scores_test['BLUE'] * 100
    writer.add_scalar("BLUE/Valid", valid_bleu_score, epoch)
    writer.add_scalar("BLUE/Test", test_bleu_score, epoch)
    _, valid_corr_score = scores_valid['Corrects']
    _, test_corr_score = scores_test['Corrects']
    writer.add_scalar("Correct/Valid", valid_corr_score * 100, epoch)
    writer.add_scalar("Correct/Test", test_corr_score * 100, epoch)

    if valid_corr_score > max_valid_corr:
        max_valid_corr = valid_corr_score
        torch.save(model.state_dict(), f'best_models/{experiment_name}_corr_best.pt')

    writer.flush()

writer.close()

model.load_state_dict(torch.load(f"best_models/{experiment_name}.pt"))
model.eval()
model.to(device)

eval_metrics = {"BLEU": bleu_score, "Corrects": corrects}

scores = compute_metrics(test_data, CODE, CODE, model, device, eval_metrics, max_len=max_lens[size])

bleu_score = scores['BLEU']
print(f'BLEU score = {bleu_score * 100:.2f}')

correct, p_correct = scores['Corrects']

print(f'Correct score = {correct}')
print(f'Correct % score = {p_correct * 100:.2f}')