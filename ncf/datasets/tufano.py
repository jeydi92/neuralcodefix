from collections import defaultdict
from os import listdir
from os.path import join, isdir, isfile

from torch.utils.data import Dataset


def read_file(path):
    with open(path, "rt", encoding="utf8") as inf:
        return inf.read().strip()


class MethodPairs(Dataset):
    def __init__(self, root_dir):
        """
        :param root_dir: Folder containing the PR subfolders
        """
        self.root_dir = root_dir
        folders = [f for f in listdir(self.root_dir) if isdir(join(self.root_dir, f))]
        self.idxs = folders

    def __len__(self):
        return len(self.idxs)

    def __getitem__(self, i):
        idx = self.idxs[i]
        code = defaultdict(lambda: {})
        path = join(self.root_dir, idx)

        files = [(f, join(path, f)) for f in listdir(path) if
                 isdir(join(path, f))]  # Folders in path represent files in source code
        for file, file_path in files:
            methods = [(f, join(file_path, f)) for f in listdir(file_path) if
                       isdir(join(file_path, f))]  # Folders in each file folder represents a method
            for method, method_path in methods:
                try:
                    code[file][method] = self.__read_files__(method_path)
                except Exception:
                    # traceback.print_exc()
                    pass
        return code

    @staticmethod
    def __read_files__(path):
        before = read_file(join(path, "before.java"))
        after = read_file(join(path, "after.java"))
        operations = read_file(join(path, "operations.txt"))
        signature = read_file(join(path, "signature.txt"))
        return {"before": before.strip(), "after": after.strip(),
                "operations": operations.strip(), "signature": signature.strip()}


class SourceAbstract(Dataset):
    """
    Reads the source-abstract pairs for the code before and after the PR. The root_dir contains the files for test, eval,
    and test set. The pairs in the files are wrote as:
        ====
        <abstracted code before>
        <abstracted code after>
        ----
        <source code before>
        ----
        <source code after>
        (empty line)

    each file contains multiple pairs.
    """

    def __init__(self, root_dir):
        self.root_dir = root_dir

        self.data = self.__read__(root_dir)

    def __len__(self):
        return len(self.data)

    def __getitem__(self, i):
        return self.data[i]

    def __read__(self, root_dir):
        code = {}
        files = [(f, join(root_dir, f)) for f in listdir(root_dir) if
                 isfile(join(root_dir, f))]

        for file, file_path in files:
            set = self.__get_set__(file)
            parsed_file = self.__parse_file__(file_path, set)
            code.update(parsed_file)

        return code

    def __parse_file__(self, file, set):
        extracted_methods = {}
        with open(file, "rt", encoding="utf8") as inf:
            method = ""
            for line in inf:
                if "====\n" in line:
                    div = 0
                    continue
                if "----\n" in line:
                    div += 1
                if len(line.strip()) == 0 and div == 3:
                    parsed = self.__parse_method__(method.strip())
                    parsed['set'] = set
                    key = (parsed['source_before'], parsed['source_after'])
                    extracted_methods[key] = parsed
                    method = ""
                    div = 0

                method += line
            if div == 3:
                parsed = self.__parse_method__(method.strip())
                parsed['set'] = set
                key = (parsed['source_before'], parsed['source_after'])
                extracted_methods[key] = parsed

        return extracted_methods

    @staticmethod
    def __parse_method__(method):
        splitted = method.strip().split("----")
        abstract_before, abstract_after = splitted[0].strip().split("\n")
        source_before = splitted[1].strip()
        source_after = splitted[2].strip()
        return {"abstract_before": abstract_before.strip(),
                "abstract_after": abstract_after.strip(),
                "source_before": source_before,
                "source_after": source_after}

    @staticmethod
    def __get_set__(file):
        if "train" in file:
            return "train"
        if "eval" in file:
            return "eval"
        if "test" in file:
            return "test"
