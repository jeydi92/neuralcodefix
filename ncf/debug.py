import random
from os.path import join

import numpy as np
import torch
from torchtext.data import Field, TabularDataset, BucketIterator
from torchtext.data.metrics import bleu_score

from inference import translate_sentence, display_attention, compute_metrics
from models.attention import Attention
from models.decoder import Decoder
from models.encoder import Encoder
from models.seq2seq import Seq2Seq
from utils.metrics import corrects

SEED = 1337

project = "ovirt"
size = "small"
# experiment_name = f"{project}-{size}-BiLSTM+Att+2LayerLSTM-BA-test2-debug"
BATCH_SIZE = 8

dataset_path = f"datasets/tufano/{project}/"

random.seed(SEED)
np.random.seed(SEED)
torch.manual_seed(SEED)
torch.cuda.manual_seed(SEED)
torch.backends.cudnn.deterministic = True

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

CODE = Field(init_token='<sos>',
             eos_token='<eos>',
             lower=True,
             include_lengths=True)

train_data = TabularDataset(
    path=join(dataset_path, f'train.json'), format='json',
    fields={'abstract_before': ('src', CODE),
            'abstract_after': ('trg', CODE)  # ,
            # 'operations': ('operations', OP)
            })

CODE.build_vocab(train_data, min_freq=1)

valid_data = TabularDataset(
    path=join(dataset_path, f'eval_{size}.json'), format='json',
    fields={'abstract_before': ('src', CODE),
            'abstract_after': ('trg', CODE)  # ,
            # 'operations': ('operations', OP)
            })

test_data = TabularDataset(
    path=join(dataset_path, f'test_{size}.json'), format='json',
    fields={'abstract_before': ('src', CODE),
            'abstract_after': ('trg', CODE)  # ,
            # 'operations': ('operations', OP)
            })

train_iterator, valid_iterator, test_iterator = BucketIterator.splits(
    (train_data, valid_data, test_data),
    batch_size=BATCH_SIZE,
    sort_within_batch=True,
    sort_key=lambda x: len(x.src),
    device=device)

INPUT_DIM = len(CODE.vocab)
OUTPUT_DIM = len(CODE.vocab)
ENC_EMB_DIM = 512
DEC_EMB_DIM = 512
ENC_HID_DIM = 256
DEC_HID_DIM = 256
ENC_DROPOUT = 0.2
DEC_DROPOUT = 0.0
SRC_PAD_IDX = CODE.vocab.stoi[CODE.pad_token]

attn = Attention(ENC_HID_DIM, DEC_HID_DIM)
enc = Encoder(INPUT_DIM, ENC_EMB_DIM, ENC_HID_DIM, DEC_HID_DIM, ENC_DROPOUT)
dec = Decoder(OUTPUT_DIM, DEC_EMB_DIM, ENC_HID_DIM, DEC_HID_DIM, DEC_DROPOUT, attn)

model = Seq2Seq(enc, dec, SRC_PAD_IDX, device)
model.load_state_dict(torch.load('BiLSTM+Att+2LayerLSTM-lr.pt'))
model.eval()
model.to(device)


max_lens = {"medium": 100, "small": 50}

def count_parameters(model):
    return sum(p.numel() for p in model.parameters() if p.requires_grad)


print(f'The model has {count_parameters(model):,} trainable parameters')

example_idx = 1

src = train_data.examples[example_idx].src
trg = train_data.examples[example_idx].trg

print(f'src = {src}')
print(f'trg = {trg}')

translation, attention = translate_sentence(src, CODE, CODE, model, device)

metrics = {"BLUE": bleu_score, "Corrects": corrects}

print(f'predicted trg = {translation}')
# print(f'Attention = {attention}')
display_attention(src, translation, attention)

scores = compute_metrics(test_data, CODE, CODE, model, device, metrics, max_len=max_lens[size])

print(str(scores))
correct, p_correct = scores['Corrects']

print(f'Correct score = {correct}')
print(f'Correct % score = {p_correct * 100:.2f}')
