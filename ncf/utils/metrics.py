def corrects(pred_trgs, trgs):
    assert len(pred_trgs) == len(trgs), "Len of predicted doesn't match true!"
    corr = 0
    n = len(pred_trgs)
    for trg, pred_trg in zip(trgs, pred_trgs):
        if trg[0] == pred_trg:
            corr += 1

    return corr, corr / n
