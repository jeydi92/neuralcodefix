import torch
from tqdm import tqdm


def train(model, iterator, optimizer, criterion, clip):
    model.train()

    epoch_loss = 0

    for batch in tqdm(iterator):
        src, src_len = batch.src
        trg, _ = batch.trg
        optimizer.zero_grad()

        output = model(src, src_len, trg)

        # trg = [trg len, batch size]
        # output = [trg len, batch size, output dim]

        output_dim = output.shape[-1]

        output = output[1:].view(-1, output_dim)
        trg = trg[1:].view(-1)

        # trg = [(trg len - 1) * batch size]
        # output = [(trg len - 1) * batch size, output dim]
        loss = criterion(output, trg)

        # TODO Check if possible to iterate over

        loss.backward()

        torch.nn.utils.clip_grad_norm_(model.parameters(), clip)

        optimizer.step()

        epoch_loss += loss.item()

    return epoch_loss / len(iterator)


def train_mtl(model, iterator, optimizer, criterion_code, criterion_op, clip):
    model.train()

    epoch_loss_both = 0
    epoch_loss_1 = 0
    epoch_loss_2 = 0

    for batch in tqdm(iterator):
        src, src_len = batch.src
        trg, _ = batch.trg
        op, _ = batch.op
        optimizer.zero_grad()

        output_code, output_op = model(src, src_len, trg, op)

        # trg = [trg len, batch size]
        # output = [trg len, batch size, output dim]

        output_dim_code = output_code.shape[-1]
        output_dim_op = output_op.shape[-1]

        output_c = output_code[1:].view(-1, output_dim_code)
        trg = trg[1:].view(-1)

        output_o = output_op[1:].view(-1, output_dim_op)
        op = op[1:].view(-1)

        # trg = [(trg len - 1) * batch size]
        # output = [(trg len - 1) * batch size, output dim]
        loss_1 = criterion_code(output_c, trg)
        loss_2 = criterion_op(output_o, op)

        total_loss = loss_1 + loss_2
        total_loss.backward()

        torch.nn.utils.clip_grad_norm_(model.parameters(), clip)

        optimizer.step()

        epoch_loss_both += loss_1.item() + loss_2.item()
        epoch_loss_1 += loss_1.item()
        epoch_loss_2 += loss_2.item()

    n = len(iterator)
    return epoch_loss_both / n, epoch_loss_1 / n, epoch_loss_2 / n


def evaluate(model, iterator, criterion):
    model.eval()

    epoch_loss = 0

    with torch.no_grad():
        for batch in tqdm(iterator):
            src, src_len = batch.src
            trg, _ = batch.trg

            output = model(src, src_len, trg, 0)  # turn off teacher forcing

            # trg = [trg len, batch size]
            # output = [trg len, batch size, output dim]

            output_dim = output.shape[-1]

            output = output[1:].view(-1, output_dim)
            trg = trg[1:].view(-1)

            # trg = [(trg len - 1) * batch size]
            # output = [(trg len - 1) * batch size, output dim]

            loss = criterion(output, trg)

            epoch_loss += loss.item()

    return epoch_loss / len(iterator)


def evaluate_mtl(model, iterator, criterion_code, criterion_op):
    model.eval()

    epoch_loss_both = 0
    epoch_loss_1 = 0
    epoch_loss_2 = 0

    with torch.no_grad():
        for batch in tqdm(iterator):
            src, src_len = batch.src
            trg, _ = batch.trg
            op, _ = batch.op

            output_code, output_op = model(src, src_len, trg, op, 0)  # turn off teacher forcing

            # trg = [trg len, batch size]
            # output = [trg len, batch size, output dim]
            output_dim_code = output_code.shape[-1]
            output_dim_op = output_op.shape[-1]

            output_c = output_code[1:].view(-1, output_dim_code)
            trg = trg[1:].view(-1)

            output_o = output_op[1:].view(-1, output_dim_op)
            op = op[1:].view(-1)

            # trg = [(trg len - 1) * batch size]
            # output = [(trg len - 1) * batch size, output dim]
            loss_1 = criterion_code(output_c, trg)
            loss_2 = criterion_op(output_o, op)

            epoch_loss_both += loss_1.item() + loss_2.item()
            epoch_loss_1 += loss_1.item()
            epoch_loss_2 += loss_2.item()

        n = len(iterator)
        return epoch_loss_both / n, epoch_loss_1 / n, epoch_loss_2 / n
