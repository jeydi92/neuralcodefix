# NeuralCodeFix

**Neural Code Fix Project for Artificial Intelligence class and exam**



### Abstract

In recent years there has been a growing interest regarding the availability and the possibility to manage and analyze open source code in public repositories. Thanks to the great availability of data and the use of Deep Learning techniques, in particular, thanks to the new "discoveries" in Natural Language Processing (NLP) to treat token sequences applicable also to the world of software engineering. Because of these two new possibilities it has been possible to achieve the first works that have managed to deal significantly with source code on different tasks such as: synthesis, interpretability, summarization, translation, etc...

The growing interest and applicability of these techniques in the field of industry and software engineering has generated also an increasing enthusiasm, for the possibility to analyse with advanced techniques, portions and segments of code, encouraging the automation and standardisation of development processes.

In this work we tackle the automatic code changes task, which consists of converting "bad" source code, extracted before a pull request, with the edited and corrected version, after the pull request. Following Tufano's work in 2019 [https://arxiv.org/abs/1901.09102] we use Neural Machine Translation techniques (NMT) to try to analyze the source code coming from google open source repositories with particular reference to Java as an object language used for analysis.

We tried to automate the process of code bugs correction starting from the code containing errors (which has problems highlighted by GitHub issue) by predicting the code to implement to correct the problem and the corresponding abstract syntax tree that allows to keep track of the changes made.

The abstract syntax tree used for the training was generated using specific tools from source code that are usually used in software engineering: GumTreeDiff. 

For this task we started using as a baseline a previous work by Tufano in 2019 based on Sequence to Sequence and reproduced their research and results. 
With respect to Tufano's work, however, it was decided to focus mainly on the quantitative and technical aspect as opposed to a more qualitative approach.

Our proposed architecture is based on an Encoder - Decoder structure, in particular One-to-many settings with an encoder, and two decoders. Where the encoder analyzes the code before and creates a dense representation of it, this representation will be used by the two decoders for their respective task. The first decoder performs the main task of predicting the output source code, as in Tufano et., the second decoder performs an auxiliary task, that is the prediction of the changes on the AST.

**Index Terms**: Neural Machine Translation, Software engineering, Seq2Seq, Source Code